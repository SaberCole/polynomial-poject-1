package edu.uprm.ece.icom4035.list;

import edu.uprm.ece.icom4035.polynomial.SinglyLinkedList;

public class SinglyLinkedListFactory<Term> implements ListFactory<Term> {

	@Override
	public List<Term> newInstance() {
		// TODO Auto-generated method stub
		return new SinglyLinkedList<Term>();
	}

}
