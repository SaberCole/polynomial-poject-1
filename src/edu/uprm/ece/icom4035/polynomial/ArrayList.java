package edu.uprm.ece.icom4035.polynomial;

import java.util.Iterator;
import java.util.NoSuchElementException;

import edu.uprm.ece.icom4035.list.List;

public class ArrayList<E> implements List<E> {
	private E[] element; 
	private int size;


	public ArrayList() { 
		element = (E[]) new Object[1]; 
		size = 0; 
	} 
	@Override
	public Iterator<E> iterator() {
		// TODO Auto-generated method stub
		return new ElementIterator<E>();
	}

	@Override
	public void add(E obj) {
		// TODO Auto-generated method stub
		if(this.size()== this.element.length){
			//reallocate
			changeCapacity(1);
		}
		this.element[this.size++]=obj;
	}

	@Override
	public void add(int index, E obj) {
		// TODO Auto-generated method stub
		if(this.size()==this.element.length){
			this.changeCapacity(1);
		}
		for(int i=this.size();i>index;--i){
			this.element[i]=this.element[i-1];
		}
		this.element[index]=obj;
		this.size++;

	}

	@Override
	public boolean remove(E obj) {
		// TODO Auto-generated method stub
		if(this.isEmpty()){
			return false;
		}
		int index=-1;
		for(int i=0; i<this.size();i++){
			if(element[i]==obj){
				index=i;	
			}
		}
		if(index==-1){
			return false; //Checking if the element is in the list
		}
		for(int i=index;i<this.size()-1;++i){
			this.element[i]=this.element[i+1];
		}
		this.element[this.size()-1]=null;
		this.size--;
		changeCapacity(-1);
		
		return true;
	}

	@Override
	public boolean remove(int index) {
		// TODO Auto-generated method stub
		if(this.isEmpty()){
			return false;
		}
		else{

			for(int i=index;i<this.size()-1;++i){
				this.element[i]=this.element[i+1];
			}
			this.element[this.size()-1]=null;
			this.size--;
			changeCapacity(-1);

			return true;
		}
	}

	@Override
	public int removeAll(E obj) {
		// TODO Auto-generated method stub
		int count=0;
		for(int i=0; i<this.size();i++){
			if(this.element[i]==obj){
				this.remove(obj);
				count++;
			}
		}
		this.size=size-count;
		return count;
	}

	@Override
	public E get(int index) {
		// TODO Auto-generated method stub
		if(index<0||index>this.size()){
			throw new IndexOutOfBoundsException("Index is out of bounds");
		}
		return this.element[index];
	}

	@Override
	public E set(int index, E obj) {
		// TODO Auto-generated method stub
		E result=this.element[index];
		this.element[index]=obj;
		return result;
	}

	@Override
	public E first() {
		// TODO Auto-generated method stub
		return element[0];
	}

	@Override
	public E last() {
		// TODO Auto-generated method stub
		return element[this.size-1];
	}

	@Override
	public int firstIndex(E obj) {
		// TODO Auto-generated method stub
		int index=0;
		for(int i=0; i<this.size()&&this.get(i)!=obj;i++){
			index++;
		}
		if(index==this.size()&&this.get(index)!=obj){
			return-1;
		}
		return index;
	}

	@Override
	public int lastIndex(E obj) {
		// TODO Auto-generated method stub
		int i=0;
		int result=-1;
		for(i=0;i<this.size();i++){
			if(element[i]==obj){
				result=i;
			}
		}
		return result;
	}

	@Override
	public int size() {
		// TODO Auto-generated method stub
		return this.size;
	}

	@Override
	public boolean isEmpty() {
		// TODO Auto-generated method stub
		return size==0;
	}

	@Override
	public boolean contains(E obj) {
		// TODO Auto-generated method stub
		for(E e: element){
			if(e==obj){
				return true;
			}
		}
		return false;
	}

	@Override
	public void clear() {
		// TODO Auto-generated method stub
		

	}
	private void changeCapacity(int change) { 
		int newCapacity = element.length + change; 
		E[] newElement = (E[]) new Object[newCapacity]; 
		for (int i=0; i<size; i++) { 
			newElement[i] = element[i]; 
			element[i] = null; 
		} 
		element = newElement; 
	}
	
		private class ElementIterator<E> implements Iterator<E> {
   // the array to iterate over
			private int current=0;
			// other internal fields...

			
			public boolean hasNext() {
				// rewrite...
				return current<element.length;   
			}

			public E next() throws NoSuchElementException {
				if (!hasNext())
					throw new 
					NoSuchElementException("No more elements to iterate over."); 
				// rewrite
				return (E) element[current++];
			}
			
		}
	}
