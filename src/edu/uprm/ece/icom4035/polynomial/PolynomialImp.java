package edu.uprm.ece.icom4035.polynomial;

import java.text.DecimalFormat;
import java.util.Iterator;
import java.util.NoSuchElementException;

import edu.uprm.ece.icom4035.list.List;

public class PolynomialImp implements Polynomial {

	private class TermIterator<Term> implements Iterator<Term>{

		private int currentIndex;


		public TermIterator() {
			this.currentIndex = 0;
		}

		@Override
		public boolean hasNext() {
			return (currentIndex < termList.size());
		}

		@Override
		public Term next() {
			while(this.hasNext()) {
				Term result = (Term) termList.get(currentIndex);
				this.currentIndex++;
				return result;
			}
			throw new NoSuchElementException();
		}


	}

	private List<Term> termList;

	public PolynomialImp() {
		this.termList = (List<Term>) TermListFactory.newListFactory().newInstance();
		for(Term t: this.termList) {
			this.termList.remove(t);

		}
	}

	public PolynomialImp(String expression) { 
		this.termList = (List<Term>) TermListFactory.newListFactory().newInstance();
		String [] splitExp = expression.split("\\+");
		double coefficient;
		int exponent;
		for(String i: splitExp) {
			if(i.contains("x")) {
				int posX = i.indexOf("x");
				if(posX == 0) {
					coefficient = 1;
				}
				else {
					coefficient = Double.parseDouble(i.substring(0,posX));
				}
				if(i.contains("^")){
					exponent = Integer.parseInt(i.substring(posX + 2));
				}
				else {
					exponent = 1;
				}
				this.termList.add(new TermImp(coefficient,exponent));
			}
			else {
				coefficient = Double.parseDouble(i);
				this.termList.add(new TermImp(coefficient, 0));
			}
		}


	}

	@Override
	public Iterator<Term> iterator() {
		return new TermIterator<Term>();
	}

	@Override
	public Polynomial add(Polynomial P2) {
		List<Term> thisList = this.termList;
		PolynomialImp p2 = (PolynomialImp) P2;
		List<Term> P2List = p2.termList;
		PolynomialImp result = new PolynomialImp();
		List<Term> resultList = result.termList;
		int i = 0;
		int j = 0;

		while(i < thisList.size() && j < P2List.size()) {
			Term ATerm = thisList.get(i);
			Term BTerm = P2List.get(j);
			double coefficient = 0;			
			if(ATerm.getExponent() == BTerm.getExponent()) {
				coefficient = ATerm.getCoefficient() + BTerm.getCoefficient();
				resultList.add(new TermImp(coefficient, ATerm.getExponent()));
				i++;
				j++;
			}

			if(ATerm.getExponent() != BTerm.getExponent()) {
				if(ATerm.getExponent() > BTerm.getExponent()) {
					resultList.add(ATerm);
					i++;
				}
				resultList.add(BTerm);
				j++;
			}
		}

		if(i< thisList.size() && j>= P2List.size()) {
			for(int l = i; l<thisList.size(); l++) {
				resultList.add(thisList.get(l));
			}
		}
		else {
			for(int l = i; l<P2List.size(); l++) {
				resultList.add(P2List.get(l));
			}
		}



		return result;
	}


	@Override
	public Polynomial subtract(Polynomial P2) {
		List<Term> polynomialToBeSubstractedList = this.termList;
		PolynomialImp polynomialToSubstract = (PolynomialImp) P2;
		List<Term> secondPolynomialList = polynomialToSubstract.termList;
		PolynomialImp polynomialSubstracted = new PolynomialImp();
		List<Term> polynomialSubstractedList = polynomialSubstracted.termList;
		int i = 0;
		int j = 0;

		while(i < polynomialToBeSubstractedList.size() && j < secondPolynomialList.size()) {
			Term TermA = polynomialToBeSubstractedList.get(i);
			Term TermB = secondPolynomialList.get(j);

			if(TermA.getExponent() == TermB.getExponent()) {
				double coefficient = 0;
				coefficient = TermA.getCoefficient() - TermB.getCoefficient();
				polynomialSubstractedList.add(new TermImp(coefficient, TermA.getExponent()));
				i++;
				j++;
			}

			if(TermA.getExponent() != TermB.getExponent()) {
				if(TermA.getExponent() > TermB.getExponent()) {
					polynomialSubstractedList.add(TermA);
					i++;
				}
				polynomialSubstractedList.add(new TermImp(-TermB.getCoefficient(), TermB.getExponent()));
				j++;
			}
		}

		if(i< polynomialToBeSubstractedList.size()) {
			for(int substractIndex = i; substractIndex<polynomialToBeSubstractedList.size(); substractIndex++) {
				polynomialSubstractedList.add(polynomialToBeSubstractedList.get(substractIndex));
			}
		}
		else {
			for(int substractIndex = i; substractIndex<secondPolynomialList.size(); substractIndex++) {
				polynomialSubstractedList.add(new TermImp(-secondPolynomialList.get(substractIndex).getCoefficient(), secondPolynomialList.get(substractIndex).getExponent()));
			}
		}



		return polynomialSubstracted;

	}


	@Override
	public Polynomial multiply(Polynomial P2) {
		PolynomialImp polynomialToMultiply = (PolynomialImp) P2;
		PolynomialImp multipliedPolynomials = new PolynomialImp();
		List<Term> multipliedPolynomialList = multipliedPolynomials.termList;
		List<Term> p2List = polynomialToMultiply.termList;
		for(Term i: this.termList) {
			for(Term j: p2List) {
				multipliedPolynomialList.add(new TermImp(i.getCoefficient()*j.getCoefficient(),
						i.getExponent() + j.getExponent()));
			}
		}
		int currpos = 0;
		for(int i = 1; i<multipliedPolynomialList.size(); i++) {
			if(multipliedPolynomialList.get(currpos).getExponent() == multipliedPolynomialList.get(i).getExponent()) {
				multipliedPolynomialList.set(currpos, new TermImp(multipliedPolynomialList.get(currpos).getCoefficient()
						+ multipliedPolynomialList.get(i).getCoefficient(), multipliedPolynomialList.get(currpos).getExponent()));
				multipliedPolynomialList.remove(i);
			}
		}


		return multipliedPolynomials;
	}

	@Override
	public Polynomial multiply(double c) {
		
		List<Term> scalarMultiplicationList = this.termList;
		
		PolynomialImp scalarMultipliedPolynomial = new PolynomialImp();
		
		List<Term> scalarMultipliedPolynomialList = scalarMultipliedPolynomial.termList;
		
		for(Term term: scalarMultiplicationList) {
			
			scalarMultipliedPolynomialList.add(new TermImp(term.getCoefficient() * c, term.getExponent()));
			
		}



		return scalarMultipliedPolynomial;
	}

	@Override
	public Polynomial derivative() {
		PolynomialImp polynomialDerivative = new PolynomialImp();
		
		List<Term> derivativeList = polynomialDerivative.termList;
		
		for(Term i: this.termList) {
			
			derivativeList.add(new TermImp(i.getCoefficient()* i.getExponent(), i.getExponent()-1));
			
		}

		return polynomialDerivative;
	}

	@Override
	public Polynomial indefiniteIntegral() {
		PolynomialImp polynomialToEvaluate = new PolynomialImp();
		
		List<Term> indefiniteIntegralList = polynomialToEvaluate.termList;
		
		for(Term term: this.termList) {
			
			indefiniteIntegralList.add(new TermImp(term.getCoefficient()/(term.getExponent()+1), term.getExponent()+1));
			
		}
		indefiniteIntegralList.add(new TermImp(1, 0));


		return polynomialToEvaluate;
	}

	@Override
	public double definiteIntegral(double intervalA, double intervalB) {
		
		PolynomialImp integralOfPolynomial = (PolynomialImp) this.indefiniteIntegral();
		
		double ValueOfA = integralOfPolynomial.evaluate(intervalA);
		
		double ValueOfB = integralOfPolynomial.evaluate(intervalB);
		
		double result = ValueOfB - ValueOfA;

		return result;
	}

	@Override
	public int degree() {
		
		return this.termList.first().getExponent();
		
	}

	@Override
	public double evaluate(double x) {
		
		double termEvaluated = 0;
		
		for(Term term: this.termList) {
			
			termEvaluated += term.evaluate(x);
			
		}
		
		return termEvaluated;
		
	}

	@Override
	public boolean equals(Polynomial P) {
		PolynomialImp p = (PolynomialImp) P;

		for(Term term1: this.termList) {

			for(Term term2: p.termList) {

				if(term1.getCoefficient() == term2.getCoefficient() && term1.getExponent() == term2.getExponent()) return true;

			}
		}
		return false;
	}

	public List<Term> getList() {
		return termList;
	}

	public void setList(List<Term> list) {
		termList = list;
	}

	@Override
	public String toString() {
		String termString = "";
		for(int termIndex = 0; termIndex <this.termList.size() -1; termIndex++) {

			if(this.termList.get(termIndex).getCoefficient()!=0){
				termString += this.termList.get(termIndex).toString() + "+";

			}
		}
		termString += this.termList.get(this.termList.size()-1);

		return termString;
	}


}