package edu.uprm.ece.icom4035.polynomial;

public interface Node<T> {
		public Object getElement();
		public void setElement(Object data);
	
}
