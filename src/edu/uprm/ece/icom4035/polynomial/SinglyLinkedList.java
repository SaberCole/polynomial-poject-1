package edu.uprm.ece.icom4035.polynomial;



import java.util.Iterator;

import edu.uprm.ece.icom4035.list.List;

public class SinglyLinkedList<E> implements List<E> {
	
	private Node<E> header; 
	
	private int length; 

	public SinglyLinkedList() { 
		
		header = new Node<E>(); 
		
		length = 0; 
		
	}
	
	@Override
	public Iterator<E> iterator() {
		
		return new ElementIterator();
		
	}
	
	private class ElementIterator implements Iterator<E> {
		
		private int position = 0;
		
		private Node<E> curr = header;

		@Override
		public boolean hasNext() {
			
			if (position < length)
				
				return true;
			
			return false;
		}

		@Override
		public E next() {
			if(hasNext()) {
				curr = curr.getNext();
				position++;
			}
			
			return curr.getElement();
		}
		
	}

	@Override
	public void add(E obj) {
		if (length == 0) header.setNext(new Node<E>(obj));
		else {
			Node<E> curr = header;
			
			while(curr.getNext() != null) {
				curr = curr.getNext();
			}
			
			curr.setNext(new Node<E>(obj));
		}
		
		length++;
	}

	@Override
	public void add(int index, E obj) {
		if (index < 0 || index > length)
			throw new IndexOutOfBoundsException("SinglyLinkedList: add() - bad index");
		
		if (length == 0) header.setNext(new Node<E>(obj));
		else {
			Node<E> curr = header;
			Node<E> next = header.getNext();
			
			for (int i = 0; i < index; i++) {
				curr = curr.getNext();
				next = curr.getNext();
			}
			
			Node<E> nuevo = new Node<E>(obj, next);
			
			curr.setNext(nuevo);
			
			length++;
		}
	}

	@Override
	public boolean remove(E obj) {
		if (length == 0) return false;
		
		Node<E> prev = header;
		Node<E> next = prev.getNext();
		
		while(next != null) {
			if (prev.getNext().getElement() != obj) {
				prev = prev.getNext();
				next = next.getNext();
			} else {
				Node<E> etr = prev.getNext();
				
				prev.setNext(next);
				
				etr.clean();
				
				length--;
				
				return true;
			}
		}
		
		return false;
	}

	@Override
	public boolean remove(int index) {
		if (index < 0 || index >= length)
			throw new IndexOutOfBoundsException("SinglyLinkedList: remove() - bad index");
		
		boolean isRemoved = 
				remove(get(index));
		
		return isRemoved;
	}

	@Override
	public int removeAll(E obj) {
		int count = 0;
		
		while(contains(obj)) {
			remove(obj);
			count++;
		}	
		
		return count;
	}

	@Override
	public E get(int index) {
		if (index < 0 || index >= length)
			throw new IndexOutOfBoundsException("SinglyLinkedList: get() - bad index");
		
		Node<E> curr = header.getNext();
		
		for (int i = 0; i < index; i++) {
			curr = curr.getNext();
		}
		
		return curr.getElement();
	}

	@Override
	public E set(int index, E obj) {
		if (index < 0 || index >= length)
			throw new IndexOutOfBoundsException("SinglyLinkedList: set() - bad index");
		
		Node<E> curr = header.getNext();
		
		for (int i = 0; i < index; i++) {
			curr = curr.getNext();
		}
		
		E old = curr.getElement();
		
		curr.setElement(obj);
		
		return old;
	}

	@Override
	public E first() {
		if (length == 0) return null;
				
		return header.getNext().getElement();
	}

	@Override
	public E last() {
		if (length == 0) return null;
		
		Node<E> curr = header;
		
		while(curr.getNext() != null)
			curr = curr.getNext();
		
		return curr.getElement();
	}

	@Override
	public int firstIndex(E obj) {
		if (length == 0) return -1;
		
		for (int i = 0; i < length; i++) {
			if (get(i) == obj)
				return i;
		}
		
		return -1;
	}

	@Override
	public int lastIndex(E obj) {
		if (length == 0) return -1;
		
		int lastIndex = -1;
		
		for (int i = 0; i < length; i++) {
			if (get(i) == obj)
				lastIndex = i;
		}
		
		return lastIndex;
	}

	@Override
	public int size() {
		return length;
	}

	@Override
	public boolean isEmpty() {
		return length == 0;
	}

	@Override
	public boolean contains(E obj) {
		return firstIndex(obj) != -1;
	}

	@Override
	public void clear() {
		header = new Node<E>();
		length = 0;
	}
	
	private static class Node<E> {
		private E element; 
		private Node<E> next; 
		public Node() { 
			element = null; 
			next = null; 
		}
		public Node(E data, Node<E> next) { 
			this.element = data; 
			this.next = next; 
		}
		public Node(E data)  { 
			this.element = data; 
			next = null; 
		}
		public E getElement() {
			return element;
		}
		public void setElement(E data) {
			this.element = data;
		}
		public Node<E> getNext() {
			return next;
		}
		public void setNext(Node<E> next) {
			this.next = next;
		}
		public void clean() {
			this.element = null;
			this.next = null;
		}
	}

}